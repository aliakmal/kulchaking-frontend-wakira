import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  public user;
  public orders;
  public loading;
  public error;

  constructor(
    private router: Router, private auth:AuthService){ 
      this.orders = [];
      this.loading = true;
    }

  ngOnInit() {
    this.user = this.auth.getLoggedInUser();
    this.auth.getOrders(this.user).subscribe(
      data=>{
        this.orders = data;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.error = true;
      },
      () => console.log('Done')
    );
  }

}
