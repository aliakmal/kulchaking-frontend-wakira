export class Address {
    id: number;
    house_no:string;
    street:string;
    address_1:string;
    address_2:string;
    zone_id: number;
    zone_name:string;
    city_id: number;
    city_name: string;
    country: string;
    is_addable:boolean;

    constructor(address){
        if(address == null){
            return;
        }

        this.id = typeof(address.id)=='undefined' ? null : address.id;
        this.house_no = typeof(address.house_no)=='undefined' ? '' : address.house_no;
        this.street = typeof(address.street)=='undefined' ? '' : address.street;
        this.address_1 = typeof(address.address_1)=='undefined' ? '' : address.address_1;
        this.address_2 = typeof(address.address_2)=='undefined' ? '' : address.address_2;
        this.zone_id = typeof(address.zone_id)=='undefined' ? null : address.zone_id;
        this.zone_name = typeof(address.zone_name)=='undefined' ? '' : address.zone_name;
        this.city_id = typeof(address.city_id)=='undefined' ? null : address.city_id;
        this.city_name = typeof(address.city_name)=='undefined' ? '' : address.city_name;
        this.country = typeof(address.country)=='undefined' ? '' : address.country;
        this.is_addable = typeof(address.id)=='undefined' ? true : false;
    }


    getDisplayable(){
        let str = [ this.house_no, 
                    this.street, 
                    this.address_1, 
                    this.address_2, 
                    this.zone_name, 
                    this.city_name];

        return str.join(', ');
    }
}

