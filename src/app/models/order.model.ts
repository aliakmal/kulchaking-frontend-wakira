import { Address } from "./address.model";
import { OrderItem } from "./order_item.model";
import { Basket } from "./basket.model";
import { CartItem } from "./cart_item.model";
import { CartComboItem } from "./cart_combo.model";
import { environment } from '../../environments/environment';
import { User } from "./user.model";

export class Order {

    id:number;
    ref:string;
    items:Array<OrderItem>;
    address:Address;

    zone_id:number;
    comments:string;
    customer_name:string;
    customer_email:string;
    customer_phone:string;

    payment_mode:string;
    payment_status:string;

    subtotal:number;
    delivery_charge:number;
    taxes:number;
    total:number;

    is_preorder:number;
    preorder_at:string;
    state:string;

    user_id:number;

    coupon_id:number;
    coupon_code:string;
    additional:string;
    applied_discount:number;

    business_id:number;

    constructor(){ 
        this.state = 'new';
        this.payment_mode = 'cash';
        this.payment_status = null;
        this.items = [];

        this.address = new Address({});

        this.coupon_id = null;
        this.coupon_code = '';
        this.applied_discount = 0;

        this.additional = '';



        this.zone_id = null;
        this.comments = '';
        this.customer_name = '';
        this.customer_email = '';
        this.customer_phone = '';
        this.subtotal = 0;
        this.delivery_charge = 0;
        this.taxes = 0;
        this.total = 0;

        this.is_preorder = 0;
        this.preorder_at = null;
        this.ref = '_';
        this.user_id = null;
        this.business_id = environment.restaurantID;


    }

    isOnlinePayment(){
        return this.payment_mode == 'card';
    }

    initForOnlinePayment(){
        this.payment_mode = 'card';
        this.payment_status = 'pending';
    }
    
    initForOfflinePayment(){
        this.state = 'pending';
    }

    attachUser(user){
        this.user_id = user.id;
        this.customer_email = user.email;
        this.customer_name = user.name;
        this.customer_phone = user.phone;
    }

    attachNonUser(user){
        this.customer_email = user.customer_email;
        this.customer_name = user.customer_name;
        this.customer_phone = user.customer_phone;
    }

    attachBasket(basket:Basket){
        for(let i in basket.items){
            let item = basket.items[i] as CartItem;
            let order_item = new OrderItem(item);
            this.items.push(order_item);
        }

        for(let i in basket.combos){
            let combo = basket.combos[i] as CartComboItem;
            let order_item = new OrderItem(combo);
            this.items.push(order_item);
        }

        this.subtotal = basket.subtotal;
        this.delivery_charge = basket.delivery_charges;
        this.total = basket.total;
    }

    applyCouponCode(params){
 
        if(params['coupon_code'] == 'kebab'){
            let item_ids_to_compare = [89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,100,101,102,103,104,105];// [61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,106,107,108,109,208,209,210,211,212,213];

            let existing_items = {};
            let count_existing_items = 0;
            let cheapest_item_id = 0;
            let cheapest_price = 10000;
            for(let i in this.items){
                if(this.items[i].item_type != 'item'){
                    continue;
                }

                //console.log(this.items[i]);
                //console.log(item_ids_to_compare.indexOf(this.items[i].item_ref.id));
                if(item_ids_to_compare.indexOf(this.items[i].item_ref.id) >=0){
                    existing_items[this.items[i].item_ref.id] = this.items[i].item_ref;
                    count_existing_items+=this.items[i].qty;
                    if(this.items[i].unit_price<cheapest_price){
                        cheapest_price = this.items[i].unit_price;
                        cheapest_item_id = this.items[i].item_ref.id;
                    }
                }
            }

            if(count_existing_items >= 2){
                // find the cheapest item in this list
                this.coupon_id = params['coupon_id'];
                this.coupon_code = params['coupon_code'];
                this.additional = 'Coupon Code '+params['coupon_code']+' applied - '+existing_items[cheapest_item_id].name + ' x 1 is FREE';
                this.applied_discount = cheapest_price;
                this.total = (this.subtotal-this.applied_discount)+this.delivery_charge;
                alert('Coupon has been applied');

            }else{
                alert('You need to select atleast two Kebabs.');
            }


        }else{
            this.coupon_id = params['coupon_id'];
            this.coupon_code = params['coupon_code'];
            this.applied_discount = params['applied_discount'];
            this.total = (this.total - this.applied_discount);
            alert('Coupon has been applied');

        }
    }
    
    resetCouponCode(){
        this.total = this.total + this.applied_discount;
        this.applied_discount = 0;
    }

    clearCouponCode(){
        this.total = this.total + this.applied_discount;
        this.coupon_id = null;
        this.coupon_code = '';
        this.applied_discount = 0;
    }

    attachAddress(address:Address){
        this.address = address;
    }
}

