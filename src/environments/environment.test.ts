export const environment = {
  production: true,
  restaurantID: 1508,
  businessName: 'Kulcha King',
  homeUrl: 'https://www.kulchaking.com',
  logoUrl:'https://s3.ap-south-1.amazonaws.com/www.kulchaking.com/images/logo.png',
  backgroundUrl:'https://s3.ap-south-1.amazonaws.com/www.kulchaking.com/images/1529081139.jpg',

  merchantID: 'TEST7005872',
  apiUrl: 'http://wakyradelvrs.ga/api',
  authCode:'bWVyY2hhbnQuVEVTVDcwMDU4NzI6Y2NjNjExM2VjYmVhNWNhMmQwM2EzNzQ1ZGRkYzRkYmQ='
};
