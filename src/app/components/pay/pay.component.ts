import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from "@angular/router";
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { NgxSmartModalService } from 'ngx-smart-modal';
import { CartItem } from '../../models/cart_item.model';
import { CartComboItem } from '../../models/cart_combo.model';
import { Basket } from '../../models/basket.model';
import { Order } from '../../models/order.model';
import { Address } from '../../models/address.model';
import { OrderService } from '../../services/order/order.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})

export class PayComponent implements OnInit {
  public order;
  public loading;
  public error;
  public error_message;

  public checkout;
  public session_id;

  constructor(
    private router: Router, public auth:AuthService, private menuSelector:MenuService, 
    public ngxSmartModalService: NgxSmartModalService,
    private cartSelector:CartService) { 
      this.loading = false;
      this.error = false;
      router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.ngOnInit();
        }
      })
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
    this.error_message = "We're sorry - but something has gone wrong, please try again later!";
    this.order = this.cartSelector.getOrder();
    this.menuSelector.getSession(this.order).subscribe(
      data => { 
        this.error = false;
        this.session_id = data['session']['id'];
      },
      err => {
        this.error = true;
        console.error(err);
      },
      () => console.log('done loading foods')
    );
    
  }

  goBackToMenu(){
    this.router.navigate(['']);
  }

  pay(){

    let amount = this.order.total;
    let ref = this.order.ref;
    (<any>window).Checkout.configure({
      merchant: this.menuSelector.getMerchantID(),/*'7005872',*/
      session:{
        id: this.session_id
      },
      order: {
        amount: function() {
          return amount;
        },
        currency: 'AED',
        description: 'Online Food Order Ref.'+ref,
        id: ref
      },

      customer: {
        email: this.order.customer_email,
        phone: this.order.customer_phone,
      },

      interaction: {
        merchant: {
          name: 'Wakira Investments',
          address: {
            line1: 'Post Box 122727',
            line2: 'Dubai'
          }
        }
      },

      billing    : {
        address: {
          street       : this.order.address.house_no + ',' + this.order.address.street,
          city         : this.order.address.zone_name,
          postcodeZip  : "",
          stateProvince: this.order.address.city_name,
          country      : "ARE",
        }
      },

  });
  (<any>window).Checkout.showLightbox();
  }
  
  errorPaymentCallback(error){
    this.error = true;
    this.error_message = "We're sorry - but the payment was not processed. You could try another card or maybe pay with cash";
  }

  cancelPaymentCallback(){
    this.error = true;
    this.error_message = "Payment was cancelled";
  }

  payByCash(){
    this.menuSelector.markOrderCashPayable(this.order).subscribe(
      data => { 
        this.error = false;
        this.cartSelector.emptyCart();

        this.router.navigate(['confirm']);
      },
      err => {
        this.error = true;
        this.error_message = 'We\'re sorry - something went wrong and your order could not be processed.';
        console.error(err)
      },
      () => console.log('done loading foods')
    );
  }



}
