import { Component,  OnInit } from '@angular/core';
import { Observable} from 'rxjs';
import { debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';
import { AuthService } from '../../services/auth/auth.service';
import { CartService } from '../../services/cart/cart.service';
import { NgVarDirective } from '../../directives/ng-var.directive';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CartItem } from '../../models/cart_item.model';
import { CartComboItem } from '../../models/cart_combo.model';
import { Basket } from '../../models/basket.model';
import { Menu } from '../../models/menu.model';
import { OrderService } from '../../services/order/order.service';
import { FormControl} from '@angular/forms';
import { startWith} from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  // ng related constructors initializers

  public show_basket_mobile;
  public menus;
  public delivery_locations;
  public basket:Basket;
  private itemInFocus:CartItem;
  private comboInFocus;
  public is_open;

  public assigned_venues;

  public is_zone_set;
  public current_zone_id;

  public current_city_id;

  public current_zone;
  public current_city;

  public current_zones;
  public timings;
  public minimum_order;

  public suggestibles;
  areaControl = new FormControl();
  filteredZones: Observable<string[]>;

  private _filterZones(value){
    const filterValue = typeof(value) == 'string' ? value.toLowerCase() : value.name.toLowerCase();
    return this.current_zones.filter(option => {
      return option.name.toLowerCase().includes(filterValue);
    });
  }

  displaySelectedZone(zone){
    if(zone!=null){
      return zone.name;
    }
  }

  setCurrentZoneID(evt: any){
    this.current_zone_id = evt.option.value.id;
  }

  constructor(private menuSelector:MenuService,
    private titleService: Title,
    public auth:AuthService,
    private router: Router, public ngxSmartModalService: NgxSmartModalService, 
    private cartSelector:CartService, private authService: AuthService) { 

    this.filteredZones = this.areaControl.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          let val = this._filterZones(value);
          return val;
        })
      );

    var now = new Date().getTime();
    var setupTime = JSON.parse(localStorage.getItem('setupTime'));
    if (setupTime == null) {
        localStorage.removeItem(this.menuSelector.getKey());
        localStorage.setItem('setupTime', JSON.stringify(now));
    } else {

        if((now-setupTime) > (12*60*60*1000)) {
            localStorage.removeItem(this.menuSelector.getKey());

        localStorage.setItem('setupTime', JSON.stringify(now));
        }
    }
  }

  ngOnInit() {
    this.show_basket_mobile = false;
    this.itemInFocus = null;
    this.comboInFocus = null;
    this.is_open = 1;
    this.minimum_order = 0;
    this.current_zone_id =  0;
    this.current_city_id = 0;
    this.current_zones = [];
    this.assigned_venues = [];
    this.timings = {};
    this.suggestibles = [];
    this.menus = [];
    this.titleService.setTitle('Order Online | '+this.auth.businessName);

    (<any>window).gtag('config', 'UA-129699975-4', {'page_path': '/'});

    this.is_zone_set = this.authService.isZoneSet();
    console.log(this.authService.getCurrentCity());
    if(this.authService.getCurrentCity()){
      this.current_city = this.authService.getCurrentCity();
      this.current_city_id = this.current_city.id;
    }

    if(this.authService.getCurrentZone()){
      this.current_zone = this.authService.getCurrentZone();
      this.current_zone_id = this.current_zone.id;
    }

    if(this.cartSelector.hasExistingCart()){
      this.basket = new Basket();
      this.basket.initFromBasket(this.cartSelector.getCart());
    }else{
      this.basket = new Basket();
      this.cartSelector.saveCart(this.basket);
    }

    this.loadDeliveryLocations();
    this.initZoneFromLocal();
  }

  isZoneSet(){
    return this.is_zone_set;
  }

  initZoneFromLocal(){
    if(typeof(this.current_zone_id)=='undefined'){
      return;
    }
    if(this.current_zone_id<=0){
      return;
    }
    this.authService.setZone(this.current_zone_id);

    this.setCurrentZoneAndCity();

    this.is_zone_set = this.authService.isZoneSet();


    this.assigned_venues = [];
    this.timings = {};
    this.menus = [];
    this.is_open = 1;

    var _dis = this;

    this.menuSelector.loadAllocatedVenuesAndMenus(this.current_zone_id).subscribe(
      data =>{
        _dis.assigned_venues = data[0];
        _dis.is_open = data[1]; 
        _dis.timings = data[2];
        
        _dis.menus = [];
        for(var ix in data[3]){
          _dis.menus.push(new Menu(data[3][ix]));
        }
        localStorage.setItem(this.menuSelector.getKey(), JSON.stringify(data[2]));
        localStorage.setItem('delivery_zone_minimum_order', JSON.stringify(data[4]));
        this.minimum_order = data[4];
      },
      err =>{
        console.log('Something went wrong try again later!');
      }
    )

  }


  setZone(){
    if(typeof(this.current_zone_id)=='undefined'){
      alert('Please select a delivery location');
      return;
    }
    if(this.current_zone_id<=0){
      alert('Please select a delivery location');
      return;
    }
    this.authService.setZone(this.current_zone_id);

    this.setCurrentZoneAndCity();

    this.is_zone_set = this.authService.isZoneSet();

    this.emptyCart();

    this.assigned_venues = [];
    this.timings = {};
    this.menus = [];
    this.is_open = 1;
    this.minimum_order = 0;

    var _dis = this;

    this.menuSelector.loadAllocatedVenuesAndMenus(this.current_zone_id).subscribe(
      data =>{
        _dis.assigned_venues = data[0];
        _dis.is_open = data[1]; 
        _dis.timings = data[2];
        
        _dis.menus = [];
        for(var ix in data[3]){
          _dis.menus.push(new Menu(data[3][ix]));
        }
        localStorage.setItem(this.menuSelector.getKey(), JSON.stringify(data[2]));
        localStorage.setItem('delivery_zone_minimum_order', JSON.stringify(data[4]));
        this.minimum_order = data[4];
      },
      err =>{
        console.log('Something went wrong try again later!');
      }
    )

  }

  setCurrentZoneAndCity(){
    for(let i in this.delivery_locations){
      if(this.current_city_id == this.delivery_locations[i].id){
        this.current_city = { id:this.delivery_locations[i].id,  name:this.delivery_locations[i].name };
        break;
      }
    }
    console.log(this.current_zone_id);
    for(let i in this.current_zones){
      if(this.current_zone_id == this.current_zones[i].id){
        this.current_zone = { id:this.current_zones[i].id,  name:this.current_zones[i].name };
        break;
      }
    }

    this.authService.setCurrentCity(this.current_city);
    this.authService.setCurrentZone(this.current_zone);

  }

  emptyCart(){
    this.basket = new Basket();
    this.cartSelector.emptyCart();
  }

  selectFirstInCurrentZones(){
    for(let i in this.delivery_locations){
      this.current_city_id = this.delivery_locations[i].id;
      this.setCurrentZonesSelectable();
      return;

    }
  }

  setCurrentZonesSelectable(){
    for(let i in this.delivery_locations){
      if(this.delivery_locations[i].id == this.current_city_id){
        this.current_zones = this.delivery_locations[i].zones;
      }

    }
  }

  loadVenues(){
    this.menuSelector.isRestaurantOpen().subscribe(
      data => { 
        this.is_open = data;
      },
      err => console.error(err),
      () => console.log('done loading foods')
    );

  }


  checkIfOpenForDelivery(){

      this.menuSelector.isRestaurantOpen().subscribe(
        data => { 
          this.is_open = data;
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
    
  }

  // menu related functions

  loadMenu(){

    this.checkIfOpenForDelivery();
    this.loadMenu();


    try{
      let results:Array<any> = JSON.parse(localStorage.getItem(this.menuSelector.getKey()));
      this.menus =  results.map(function(menu_item){
        return new Menu(menu_item);
      });
    }catch(e){

      this.menuSelector.getRestaurantMenu().subscribe(
        data => { 
          this.menus = data;
          localStorage.setItem(this.menuSelector.getKey(), JSON.stringify(data));
          
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
    }
    
  }

  loadDeliveryLocations(){
    try{
      let results = JSON.parse(localStorage.getItem(this.menuSelector.getDeliveryLocationsKey()));
      if(!results.isArray()){
        throw results;
      }
      this.delivery_locations = results;
      this.selectFirstInCurrentZones();

    }catch(e){
      this.menuSelector.getDeliveryLocations().subscribe(
        data => { 
          this.delivery_locations = data;
          this.setCurrentZonesSelectable();
          this.selectFirstInCurrentZones();
          localStorage.setItem(this.menuSelector.getDeliveryLocationsKey(), JSON.stringify(data));
        },
        err => {console.error('err');console.error(err);},
        () => console.log('done loading')
      );

    }
   
  }

  

  searchAllZones = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.delivery_locations.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )  

  // modal functions

  hasItemInFocus(){
    return this.itemInFocus == null ? false : true;
  }

  hasComboInFocus(){
    return this.comboInFocus == null ? false : true;
  }

  generateArray(obj){
    return Object.keys(obj).map((key)=>{ return {key:key, value:obj[key]}});
  }

  cartIsEmpty(){
    return (this.getCartTotalCount()) == 0 ? true : false;
  }

  getCartTotalCount(){
    return this.basket.count();
  }

  getItemableFromItem(item){
    let itemable = item;
    itemable['qty'] = 1;
    itemable['option'] = null;
    return itemable;
  }

  getComboableFromCombo(combo){
    let comboable = combo;
    comboable['qty'] = 1;
    comboable['options'] = {};
    return comboable;
  }
  
  openItemDetails(item){
    if(this.is_open == 0){
      alert('Sorry - the restaurant is closed now.');return;
    }
    let itemInFocus = new CartItem();
    itemInFocus.initFromItem(item);
    itemInFocus.selectFirstVariation();
    this.itemInFocus = itemInFocus;
    this.ngxSmartModalService.setModalData(this.itemInFocus, 'itemModal');
    this.ngxSmartModalService.getModal('itemModal').open();
  }

  openBasketModal(){
    this.show_basket_mobile = true;
    this.ngxSmartModalService.getModal('basketModal').open();
    
  }




  toggleOptionCombo(comboInFocus, option_name, option){
    comboInFocus['options'][option_name] =  option;
  }
  
  closeItemDetails(){
    this.itemInFocus = null;
  }

  openComboDetails(combo){
    if(this.is_open == 0){
      alert('Sorry - the restaurant is closed now.');return;
    }
    this.comboInFocus = new CartComboItem();
    this.comboInFocus.initFromCombo(combo);
    this.ngxSmartModalService.setModalData(this.comboInFocus, 'comboModal');
    this.ngxSmartModalService.getModal('comboModal').open();
  }

  closeComboDetails(){
    this.comboInFocus = null;
  }

  // cart related functions

  addItemToCart(item){
    if(this.is_open == 0){
      alert('Sorry - the restaurant is closed now.');return;
    }
    this.basket.addItem(item);
    this.basket.outputItemSelecteds();
    this.ngxSmartModalService.getModal('itemModal').close();    
    (<any>window).gtag('event', 'add_item_to_cart', {'event_category':'online_ordering'});

  }

  addSuggestionToCart(item){
    if(this.is_open == 0){
      alert('Sorry - the restaurant is closed now.');return;
    }
    this.basket.addItem(item);
    this.basket.outputItemSelecteds();
    this.ngxSmartModalService.getModal('suggestionsModal').close();    
    (<any>window).gtag('event', 'add_suggestion_to_cart', {'event_category':'online_ordering'});

    this.cartSelector.saveCart(this.basket);
    this.router.navigate(['checkout']);

  }

  resetItemInFocus(){
    this.itemInFocus = null;
  }

  proceedToCheckout(){
    this.suggestibles = [];
  
    if(this.basket.subtotal < this.minimum_order){
      alert('Please make a minimum order of AED '+this.minimum_order+' for delivery to '+this.current_zone.name);
      return false;
    }

    let _category_ids_in_menu = [];
    for(let i = 0; i < this.menus.length; i++){
      for(let y = 0; y < this.menus[i].sections.length; y++){
        _category_ids_in_menu.push(this.menus[i].sections[y].id);
      }
    }
 
    let suggested_category = this.basket.checkSuggestives(_category_ids_in_menu);

    console.log(suggested_category);
    if(suggested_category!=false){
      let suggestibles = [];
      let suggestible_category = {};
      for(let i = 0; i < this.menus.length; i++){
        for(let y = 0; y < this.menus[i].sections.length; y++){

          if(this.menus[i].sections[y].id == suggested_category){
            suggestible_category = this.menus[i].sections[y];
            //for(let z = 0; (z < this.menus[i].sections[y].items.length); z++){
            for(let z = (this.menus[i].sections[y].items.length - 1); (z >=0); --z){
              console.log(this.menus[i].sections[y].items);
              console.log(this.menus[i].sections[y].items.length);
              console.log(z);
              if(suggestibles.length > 2){
                continue;
              }
      
              let _item  = new CartItem();
              _item.initFromItem(this.menus[i].sections[y].items[z]);

              suggestibles.push(_item);
            }
          }
        }
      }
      if(suggestibles.length > 0){
        this.suggestibles = suggestibles;
        this.ngxSmartModalService.setModalData(suggestibles, 'suggestionsModal');
        this.ngxSmartModalService.getModal('suggestionsModal').open();

        return;
      }

      console.log({suggestibles:suggestibles});
    }

    (<any>window).gtag('event', 'proceed_to_checkout', {'event_category':'online_ordering'});

    this.cartSelector.saveCart(this.basket);
    this.router.navigate(['checkout']);
  }

  closeSuggestionsDetails(){
    this.cartSelector.saveCart(this.basket);
    this.router.navigate(['checkout']);
  }


  getCartItems(){
    return this.basket.items;
  }


  addComboToCart(combo){
    if(this.is_open == 0){
      alert('Sorry - the restaurant is closed now.');return;
    }
    if(combo.validate()==false){
      return;
    }
    this.basket.addCombo(combo);
    this.ngxSmartModalService.getModal('comboModal').close();    
  }


}
