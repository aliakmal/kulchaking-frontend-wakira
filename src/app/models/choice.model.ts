import { SelectOption } from './select_option.model'

export class Choice {
    name: string;
    options:string;
    selectable_options:Array<SelectOption>;
    constructor(choice){
        this.name = choice.name;
        this.options = choice.options;
        this.selectable_options = [];

        let optionables = choice['options'].split(',');
        for(let j in optionables){
            let option = new SelectOption();
            if(typeof(optionables[j])!='string'){
                continue;
            }
            option['name'] = optionables[j].split('@')[0];
            if(optionables[j].split('@').length > 1){
                option['price'] = optionables[j].split('@')[1];
            }else{
                option['price'] = 0;
            }
            option['option_of'] = this.name;
            this.selectable_options.push(option);
        }
    }
}

