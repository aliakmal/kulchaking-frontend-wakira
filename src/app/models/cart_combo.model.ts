import { SlugifyPipe } from '../shared/slugify.pipe'
import { ItemVariation } from './item_variation.model'
import { ComboItem } from './combo_item.model'
import { ComboOption } from './combo_option.model'
export class CartComboItem {
    name: string;
    id: string;
    description: string;
    combo: ComboItem;
    selected_options:Array<ComboOption>;
    unit_price: number;
    subtotal_price: number;
    qty: number;
    type:string;
    formatted_extras:string;
    item: ComboItem;

    errors:Array<string>;

    private slugifyPipe;

    constructor(){
    }


    initFromBasketCombo(cart_combo:CartComboItem){
        this.combo = new ComboItem(cart_combo.combo);
        this.name = cart_combo.name;
        this.description = cart_combo.description;
        this.slugifyPipe = new SlugifyPipe();
        this.qty = cart_combo.qty;
        this.type = 'combo';
        this.selected_options = [];
        for(let i in cart_combo.selected_options){
            let combo_option = new ComboOption(cart_combo.selected_options[i]);
            this.selected_options.push(combo_option);
        }
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();


    }



    initFromCombo(combo:ComboItem){
        this.combo = combo;
        this.name = combo.name;
        this.description = combo.description;
        this.slugifyPipe = new SlugifyPipe();
        this.qty = 1;
        this.errors = [];
        this.type = 'combo';
        this.selected_options = [];
        this.initializeSelectedOptions();
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
    }

    getType(){
        return 'combo';
    }

    getFormattedExtras(){
        return this.selected_options.map(res=>{ return res.option_of+': '+res.name+''}).join(', ');
    }
    updateFormattedExtras(){
        this.formatted_extras = this.getFormattedExtras();
    }


    initializeSelectedOptions(){
        for(let i in this.combo.selectables){
            let combo_option = new ComboOption({option_of:this.combo.selectables[i].name, name:null, price:0 });
            this.selected_options.push(combo_option);
        }
    }


    decrement(num){
        this.qty = this.qty <=1 ? 1 : (this.qty - 1);
        this.updatePrices();
    }

    increment(num){
        this.qty = this.qty + num;
        this.updatePrices();
    }

    selectOption(option){
        for(let i in this.selected_options){
            if(this.selected_options[i].option_of == option.option_of){
                this.selected_options[i] = new ComboOption(option);
            }
        }
        
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
    }

    addError(error){
        this.errors.push(error);
    }

    clearErrors(){
        this.errors = [];
    }

    hasErrors(){
        return this.errors.length > 0;
    }

    validate(){
        this.clearErrors();
        let requireds = [];
        for(let i in this.selected_options){
            if(this.selected_options[i].name == null){
                requireds.push(this.selected_options[i].option_of);
            }
        }

        if(requireds.length > 0){
            this.addError('Select atleast one '+(requireds.join(', ')));
        }

        return this.hasErrors() == false;
    }


    updatePrices(){
        this.updateUnitPrice();
        this.updateSubTotalPrice();
    }

    updateLocalId(){
        let selected_options = this.selected_options.map(res=>{return res.name});
        this.id = this.slugifyPipe.transform(this.combo.name+' '+selected_options.join(' '));
    }

    updateUnitPrice(){
        this.unit_price = this.getComboPrice()
    }

    updateSubTotalPrice(){
        this.subtotal_price = this.unit_price*this.qty;
    }

    isSelectedOption(option){
        for(let i in this.selected_options){
            if(option.option_of == this.selected_options[i].option_of){
                if(this.selected_options[i].name == option.name){
                    return true;
                }
            }
        }

        return false;
    }

    toggleOption(option){
        for(let i in this.selected_options){
            if(option.option_of == this.selected_options[i].option_of){
                this.selected_options[i] = new ComboOption(option);
            }
        }
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
        
    }

    getComboPrice(){
        let price = +this.combo.base_price;
        if(this.selected_options.length > 0){
            for(let i in this.selected_options){
                price = +this.selected_options[i].price + price;
            }
        }

        return +price;
    }

}

