import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';

import { Menu } from '../../models/menu.model';
import { ItemVariation } from '../../models/item_variation.model';

import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class OrderService {
  url:string;
  restaurantID:number;
  cities;
  loading:boolean;

  constructor(private http:HttpClient) { 
    this.cities = [];
    this.loading = false;

    this.url = environment.apiUrl;
    this.restaurantID = environment.restaurantID;
  }

  getDeliveryLocationsKey(){
    return 'delivery_locations_'+this.restaurantID;
  }

  getDeliveryLocations(){
    let key = this.getDeliveryLocationsKey();
    let data = 'restaurant_id='+this.restaurantID;
    let url = this.url+'/delivery_locations?'+data;
    let results = this.http.get(url);
    return results;
  }




}
