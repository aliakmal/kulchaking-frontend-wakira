import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { NgxSmartModalService } from 'ngx-smart-modal';
import { CartItem } from '../../models/cart_item.model';
import { CartComboItem } from '../../models/cart_combo.model';
import { Basket } from '../../models/basket.model';
import { Order } from '../../models/order.model';
import { Address } from '../../models/address.model';
import { OrderService } from '../../services/order/order.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})

export class CheckoutComponent implements OnInit {
  public basket:Basket;
  public order:Order;
  public address:Address;
  public addresses;//:Array<Address>;
  public delivery_locations;
  public current_zones;
  public loading;
  public error;
  public showAddressForm;

  public error_message;
  public minimum_order;
  public current_city;
  public current_zone;

  constructor(
    private router: Router, private auth:AuthService, private menuSelector:MenuService, public ngxSmartModalService: NgxSmartModalService,
    private cartSelector:CartService) { 
      this.loading = false;
      this.error = false;
      this.addresses = [];
      this.showAddressForm = true;
    }

  ngOnInit() {
    this.loading = false;
    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
    this.basket = this.cartSelector.getCart();
    this.minimum_order = localStorage.getItem('delivery_zone_minimum_order');
    this.error_message = '';
    this.order = new Order();
    if(this.auth.isLoggedIn()){
      this.order.user_id = this.auth.getUserId();
    }
    
    this.order.attachBasket(this.basket);
    this.current_city = this.auth.getCurrentCity();
    this.current_zone = this.auth.getCurrentZone();

    if(this.auth.isLoggedIn()){
      let user = this.auth.getLoggedInUser();
      this.order.attachUser(user);

      this.auth.getAddresses(user).subscribe(
        data => { 
          for(let i in data){
            if(this.current_zone.name == data[i].zone_name){
              this.addresses.push(new Address(data[i]));
            }
          }
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
      if(this.addresses.length > 0){
        this.showAddressForm = false;
      }
    }else{
      if(this.getLastContact()){
        let last_contact = this.getLastContact();
        this.order.attachNonUser(last_contact);
      }
    }

    this.addInitalAddress();

    this.current_zones = [];
    this.delivery_locations = [];


    this.loadDeliveryLocations();
    (<any>window).gtag('config', 'UA-129699975-4', {'page_path': '/checkout'});

  }

  saveLastAddress(order){
    localStorage.setItem('last_address', JSON.stringify(order.address));
    localStorage.setItem('last_contact', JSON.stringify({
                            customer_name:order.customer_name,
                            customer_email:order.customer_email,
                            customer_phone:order.customer_phone
                          }));
    
  }

  getLastContact(){
    let last_contact = localStorage.getItem('last_contact');
    if(!last_contact){
      return false;
    }

    var results = JSON.parse(last_contact);
    if(results.customer_name == ''){
      return false;
    }

    return results;
  }

  getLastAddress(){
    let last_address = localStorage.getItem('last_address'); 
    if(!last_address){
      return false;
    }

    var results = JSON.parse(last_address);
    if(results.zone_name == ''){
      return false;
    }

    console.log(this.current_zone);
    console.log(results);

    if(this.current_zone.name!=results.zone_name){
      return false;
    }

    return results;
  }

  addEmptyAddress(){
    this.order.address = new Address({
      zone_id:this.current_zone.id,
      zone_name:this.current_zone.name,
      city_id:this.current_city.id,
      city_name:this.current_city.name,
      country:'AE'
    });

    this.showAddressForm = true;
  }

  addInitalAddress(){
    let last_address = this.getLastAddress();
    if(!last_address){

    }else{
      this.order.attachAddress(new Address(last_address));
    }

    this.showAddressForm = true;
  }

  goBackToMenu(){
    this.router.navigate(['']);
  }

  setCurrentZonesSelectable(){
    for(let i in this.delivery_locations){
      if(this.delivery_locations[i].id == this.order.address.city_id){
        this.current_zones = this.delivery_locations[i].zones;
      }

    }
  }

  applyCouponToOrder(){
    if(this.order.coupon_code == 'kul9'){
      if(this.order.address.zone_id == 385){
        alert('Coupon not applicable to current delivery location');
        return;
      }
    }

    let params = {};
    this.order.resetCouponCode();
    this.cartSelector.saveOrder(this.order);
    params['coupon'] = this.order.coupon_code;
    params['subtotal'] = this.order.subtotal;
    params['phone'] = this.order.customer_phone;

    if(!this.auth.isLoggedIn()){
      params['user_id'] = 0;
      if(!this.validatePhone()){
        alert('Please input a valid UAE mobile number first');
        return;
      }
    }else{
      params['user_id'] = this.auth.getUserId();
    }


    this.menuSelector.applyCouponToOrder(params).subscribe(
      data => { 
        if(data['success'] == 0){
          this.order.clearCouponCode();
          alert(data['message']);
          return;
        }
        this.order.applyCouponCode(data);
        this.cartSelector.saveOrder(this.order);
        (<any>window).gtag('event', 'coupon_applied_'+params['coupon'], {'event_category':'online_ordering'});
        return;
      },
      err => {
        this.order.clearCouponCode();
        this.cartSelector.saveOrder(this.order);
        alert('Something went wrong - coupon could not be applied');
      },
      () => console.log('done loading foods')
    );
  }

  initializeAddress(){
    let last_address = this.getLastAddress();
    if(!last_address){
      this.order.address = new Address({
        zone_id:this.current_zone.id,
        zone_name:this.current_zone.name,
        city_id:this.current_city.id,
        city_name:this.current_city.name,
        country:'AE'
      });
    }else{
      this.order.attachAddress(new Address(last_address));
    }


    this.current_zones = this.delivery_locations[0].zones;
  }

  loadDeliveryLocations(){
    let results = localStorage.getItem(this.menuSelector.getDeliveryLocationsKey());
    if(!results)
    {
      this.menuSelector.getDeliveryLocations().subscribe(
        data => { 
          this.delivery_locations = data;
          this.initializeAddress();
          localStorage.setItem(this.menuSelector.getDeliveryLocationsKey(), JSON.stringify(data));
          
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
    }else{
      results = JSON.parse(results);
      this.delivery_locations = results;
      this.initializeAddress();
    }
   
  }

  isOnlinePayment(){
    return this.order.isOnlinePayment();
  }

  initForOnlinePayment(){
    this.order.initForOnlinePayment();
  }

  removeAddress(address){
    for(let i in this.addresses){
      if(this.addresses[i].id == address.id){
        this.auth.removeAddress(address.id).subscribe(
          data => { 
            this.addresses.splice(+i, 1);
            this.auth.refreshUserDetails();
          },
          err => console.error(err),
          () => console.log('done loading foods')
        );
        return;
      }
    }
  }

  setCurrentAddress(address){
    this.order.address = new Address(address);
    this.setCurrentZonesSelectable();
    
    this.showAddressForm = true;
  }

  validate(){
    let str = {  house_no:'House Number', 
                street:'Street', 
                address_1:'Address 1', 
              city_id:'City', 
              zone_id:'Area'};
    var errors = [];
    this.error_message = '';
    
    for(let i in str){
      if(this.order.address[i] == ''){
        errors.push(str[i]);
      }
    }

    let str2 = {'customer_name':'Customer Name', 
            'customer_phone':'Mobile Phone'};

    for(let j in str){
      if(this.order[j] == ''){
        errors.push(str2[j]);
      }
    }

    if(errors.length > 0){
      this.error_message = errors.join(',')+' cannot be empty';
    }

    //var regex_phone = /^(?:\+971|00971|0)(?:50|51|52|55|56)[0-9]{7}$/;
    var regex_phone = /^(?:0)(?:50|51|52|55|56|54|57)[0-9]{7}$/;

    if(!this.validatePhone()){
      this.error_message = this.error_message+' Phone must be a valid UAE mobile phone of the format 05########';
    }

    if(this.error_message != ''){
      alert(this.error_message);
      errors = [];
      return false;
    }else{
      errors = [];
      this.error_message = '';
    }

    return true;
  }

  validatePhone(){
    var regex_phone = /^(?:0)(?:50|51|52|55|56|54|57)[0-9]{7}$/;

    if(!regex_phone.test(this.order['customer_phone'])){
      return false;
    }

    return true;

  }

  confirmOrder(){
    if((this.order.subtotal-this.order.applied_discount) < this.minimum_order){
      alert('Please make a minimum order of AED '+this.minimum_order+' for delivery to '+this.current_zone.name);
      return false;
    }


    this.order.zone_id = this.order.address.zone_id;
    if(this.validate()==false){
      this.error = true;

      return;
    }

    this.loading = true;

    if(this.isOnlinePayment()){
      this.initForOnlinePayment();
    }else{
      this.order.initForOfflinePayment();
    }

    this.saveLastAddress(this.order);

    this.menuSelector.saveOrder(this.order).subscribe(
      data=>{
        this.loading = false;
        this.error = false;

        this.cartSelector.saveOrder(data);
        if(this.isOnlinePayment()){
          this.router.navigate(['pay']);
          (<any>window).gtag('event', 'order_pending_online_payment', {'event_category':'online_ordering'});
        }else{
          this.cartSelector.emptyCart();
          (<any>window).gtag('event', 'order_confirmed_card_cash', {'event_category':'online_ordering'});
          this.router.navigate(['confirm']);
        }
      },
      err => {
        this.loading = false;
        console.log('err');
        this.error = true;
        console.log(err);
      },
      () => console.log('Done')
    );
  }

}
