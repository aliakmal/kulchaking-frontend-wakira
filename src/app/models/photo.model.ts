export class Photo {
    id: number;
    original_image_path: string;
    medium_image_path: string;
    small_image_path: string;
    created_at: string;
    updated_at: string;
    constructor(photo){
        if(photo == null){
            return;
        }
        this.id = photo.id;
        this.original_image_path = photo.original_image_path;
        this.medium_image_path = photo.medium_image_path;
        this.small_image_path = photo.small_image_path;
        this.created_at = photo.created_at;
        this.updated_at = photo.updated_at;
            
    }
}

