export class ComboOption {
    name: string;
    option_of: string;
    price: string;

    constructor(combo_option){
        this.name = combo_option.name;
        this.option_of = combo_option.option_of;
        this.price = combo_option.price;
    }
}

