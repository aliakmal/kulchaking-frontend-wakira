import { Address } from "./address.model";
import {Order } from "./order.model";
export class User {
    id: number;
    name: string;
    phone: string;
    email: string;
    password: string;
    confirm_password:string;
    addresses:Array<Address>
    orders:Array<Order>
    token:string;

    constructor(){
        this.name = '';
        this.email = '';
        this.phone = '';
        this.password = '';
        this.confirm_password = '';

        this.addresses = [];
        this.orders = [];
    }

    initFromObject(user){
        if(user == null){
            return;
        }
        this.id = user.id;
        this.name = user.name;
        this.email = user.email;        
        this.phone = user.phone;
        if(typeof(user.addresses) == 'undefined'){
            this.addresses = [];
        }else{
            this.addresses = user.addresses.map(function(address){
                return new Address(address);
            });
        }
        if(typeof(user.orders) == 'undefined'){
            this.orders = user.orders;
        }else{
            this.orders = [];
        }
    }


}

