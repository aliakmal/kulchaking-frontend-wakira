import { Injectable } from '@angular/core';
import { Basket } from '../../models/basket.model';

@Injectable()
export class CartService {

  private cart = {};

  constructor() {
    this.cart['items'] = {};
    this.cart['combos'] = {};
  }

  emptyCart(){
    this.cart = {};
    let b = new Basket();
    this.saveCart(b);
  }

  addItem(item){
    if(typeof(this.cart['items']) == 'undefined'){
      this.cart['items'] = {};
    }

    if(typeof(this.cart['items'][item.id]) == 'undefined'){
      this.cart['items'][item.id] = [];
      this.cart['items'][item.id] = item;
    }else{
      this.cart['items'][item.id].increment(item.qty);
    }
  }

  getBasketID(){
    return 'current_basket_user_kk';
  }

  getOrderID(){
    return 'current_order_user_kk';
  }

  hasExistingCart(){
    return !localStorage.getItem(this.getBasketID())?false:true;
  }
  
  getCart(){
    return  JSON.parse(localStorage.getItem(this.getBasketID()));
  }

  isEmpty(){
    return this.getTotalCount() > 0 ? false : true;
  }



  getTotalCount(){
    return this.getCountOfItems() + this.getCountOfCombos();
  }

  getCountOfItems(){
    let count_items = 0;

    for(let i of this.cart['items']){
      count_items+=this.cart['items'][i]['qty'];
    }

    return count_items;
  }

  getCountOfCombos(){
    let count_combos = 0;

    for(let i of this.cart['combos']){
      count_combos+=this.cart['combos'][i]['qty'];
    }

    return count_combos;
  }

  getPriceOfItemsAtIndex(index){
    let price = 0;
    if(typeof(this.cart['items'][index]) == 'undefined'){
      return price;
    }
    let itm = this.cart['items'][index];
    if(itm.variations.length ==0){
      return itm.base_price*itm.qty;
    }else{
      for(let variation of itm.variations){
        if(itm.option == variation.name){
          return variation.price*itm.qty;
        }
      }
    }
  }

  getItems(){
    return this.cart['items'];
  }

  getCombos(){
    return this.cart['combos'];
  }

  removeItem(item){
    if(item.id in this.cart['items']){
      this.cart['items'][item.id]['qty']--;
    }
  }

  saveCart(cart){
    localStorage.setItem(this.getBasketID(), JSON.stringify(cart));
  }



  getOrder(){
    return JSON.parse(localStorage.getItem(this.getOrderID()));
  }

  saveOrder(order){
    localStorage.setItem(this.getOrderID(), JSON.stringify(order));
  }
}
