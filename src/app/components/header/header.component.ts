import { Component, NgZone, HostListener, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public isLoggedIn;  
  public loading;
  navbarOpen = false;
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  @HostListener('document:payment-error', ['$event', '$event.detail.error'])
  paymentError(event, error){
    console.log(error);
    this.auth.setGatewayError('We\'re sorry but something went wrong - try again later or another payment mode.');
    (<any>window).gtag('event', 'order_error_online', {'event_category':'online_ordering'});

  }


  @HostListener('document:payment-cancel', ['$event'])
  paymentCancel(event){
    this.auth.setGatewayError( 'The transaction was cancelled');
    (<any>window).gtag('event', 'order_cancelled_online', {'event_category':'online_ordering'});
  }

  @HostListener('document:payment-success', ['$event.detail.resultIndicator', '$event.detail.sessionVersion'])
  paymentSuccess(event, resultIndicator, sessionVersion){
    var _self = this;
    this.loading = true;
    this.menuSelector.markOrderCardPaid(_self.cartSelector.getOrder(), { resultIndicator:resultIndicator, sessionVersion:sessionVersion }).subscribe(
      data => { 
        (<any>window).gtag('event', 'order_success_online', {'event_category':'online_ordering'});
        _self.auth.setGatewayError('');
        _self.ngZone.run( () => {
            _self.cartSelector.emptyCart();
            _self.loading = false;
            _self.router.navigate(['confirm']);
        });
      },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }

  constructor(public auth:AuthService, 
        private ngZone: NgZone,    

    private cartSelector:CartService,
    private menuSelector:MenuService, 
    private router:Router) {

    var _self = this;
    this.loading = false;

    this.isLoggedIn = this.auth.isLoggedIn();
  }

  ngOnInit() {
    this.isLoggedIn = this.auth.isLoggedIn();
  }

  logout(){
    this.auth.logout();
    this.router.navigate(['']);
  }

}
