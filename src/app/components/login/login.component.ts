import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loading;
  public error;
  public errors;
  public user:User;

  constructor(
    private router: Router, private auth:AuthService) { 
      this.loading = false;
      this.error = false;
      this.errors = [];
  }


  ngOnInit() {
    this.user = new User();
  }
  generateArray(obj){
    return Object.keys(obj).map((key)=>{ return {key:key, value:obj[key]}});
  }

  login(){
    this.loading = true;
    this.errors = [];
    this.error = false;
    this.auth.login(this.user).subscribe(
      data=>{
        this.loading = false;
        this.auth.logUserIn(data['success']);
        this.router.navigate(['']);
      },
      err => {
        this.loading = false;
        this.error = true;
        this.errors = err.error.error;
      },
      () => console.log('Done')
    );
  }

}
