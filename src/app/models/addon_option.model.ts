export class AddonOption {
    name: string;
    option_of: string;
    price: string;

    constructor(addon_option){
        this.name = addon_option.name;
        this.option_of = addon_option.option_of;
        this.price = addon_option.price;
    }
}

