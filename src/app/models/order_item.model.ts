import { CartItem } from "./cart_item.model";
import { CartComboItem } from "./cart_combo.model";

export class OrderItem {
    id: number;
    item:string;
    item_ref;
    item_extras:string;
    item_type:string;
    item_id:number;
    unit_price:number;
    qty:number;
    subtotal:number;
    
    constructor(item:CartItem|CartComboItem){
        this.id = +item.item.id;
        this.item = item.name;
        this.item_ref = item.item;
        this.item_id = +item.item.id;
        this.item_type = item.type;
        this.item_extras = item.formatted_extras;
        this.unit_price = item.unit_price;
        this.subtotal = item.subtotal_price;
        this.qty = item.qty;
    }
}

