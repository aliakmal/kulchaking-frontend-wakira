import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";
import 'rxjs';
import { User } from '../../models/user.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpPostOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
};

@Injectable()
export class AuthService {
  public user;
  public loading;
  public err;
  public url;
  public restaurantID;

  public businessName;
  public homeUrl;
  public logoUrl;
  public bgUrl;

  public gateway_error;

  constructor(private http:HttpClient) { 
    this.loading = false;
    this.url = environment.apiUrl;
    this.restaurantID = environment.restaurantID;

    this.businessName = environment.businessName;
    this.homeUrl = environment.homeUrl;
    this.logoUrl = environment.logoUrl;
    this.bgUrl = environment.backgroundUrl;

    this.gateway_error = '';
  }

  setGatewayError(str){
    this.gateway_error = str;
  }

  clearGatewayError(){
    this.gateway_error = '';
  }

  getGatewayError(){
    return this.gateway_error;
  }

  hasGatewayError(){
    return this.gateway_error == '' ? false : true;
  }

  register(user){
    let url = this.url+'/register';
    let response = this.http.post(url, {data:user}, httpPostOptions);
    return response;
  }

  login(user){
    let url = this.url+'/login';
    let response = this.http.post(url, {data:user}, httpPostOptions);
    return response;
  }

  refreshUserDetails(){
    let user_id = this.getUserId()
    let url = this.url+'/user-details?user_id='+user_id;
    this.http.get(url).subscribe(
      data=>{
        this.logUserIn(data['success']);
      },
      err => {
        console.log(err);
      },
      () => console.log('Done')
    );
  }


  getKey(){
    return 'user-auth';
  }

  logout(){
    localStorage.removeItem(this.getKey());
  }

  isLoggedIn(){
    if(!localStorage.getItem(this.getKey())){
      return false;
    }else{
      return true;
    }
  }

  getUserId(){
    let user = this.getLoggedInUser();
    return user['id'];
  }

  getLoggedInUser(){
    let data = JSON.parse(localStorage.getItem(this.getKey()));
    let user =  new User();
    user.initFromObject(data);

    return user;
  }


  getOrders(user){
      let url = this.url+'/orders?user_id='+user.id;
      let response = this.http.get(url);
      return response;
  }

  getAddresses(user){
    let url = this.url+'/addresses?user_id='+user.id;
    let response = this.http.get(url);
    return response;
  }

  logUserIn(user){
    localStorage.setItem(this.getKey(), JSON.stringify(user));
  }

  removeAddress(address_id){
    let url = this.url+'/addresses/delete';
    let response = this.http.post(url, {data:{ id:address_id}}, httpPostOptions);
    return response;
  }

  getDeliveryZonesKey(){
    return 'delivery_zones_kk'+this.getHash();
  }

  isZoneSet(){
    if(!localStorage.getItem(this.getDeliveryZonesKey())){
      return false;
    }

    if(typeof(localStorage.getItem(this.getDeliveryZonesKey())) == 'undefined'){
      return false
    }
    return true;
  }

  setZone(zone_id){
    if(typeof(zone_id)=='undefined'){
      return;
    }
    localStorage.setItem(this.getDeliveryZonesKey(), zone_id);

  }

  getZone(){
    return localStorage.getItem(this.getDeliveryZonesKey());
  }

  setVenues(){

  }

  getHash(){
    return 'sss';
  }

  getCurrentCityKey(){
    return 'current_city_kk'+this.getHash();
  }

  getCurrentZoneKey(){
    return 'current_zone_kk'+this.getHash();
  }

  setCurrentCity(city){
    if(typeof(city)=='undefined'){
      return;
    }
    localStorage.setItem(this.getCurrentCityKey(), JSON.stringify(city))
  }

  setCurrentZone(zone){
    if(typeof(zone)=='undefined'){
      return;
    }
    localStorage.setItem(this.getCurrentZoneKey(), JSON.stringify(zone))
  }

  getCurrentCity(){
    let currentCity = localStorage.getItem(this.getCurrentCityKey());
    if(!currentCity){
      return false;
    }else{
      return JSON.parse(currentCity);
    }
  }

  getCurrentZone(){
    let currentZone = localStorage.getItem(this.getCurrentZoneKey());
    if(!currentZone){
      return false;
    }else{
      return JSON.parse(currentZone);
    }
  }



}
