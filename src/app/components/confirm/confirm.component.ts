import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  public order;
  constructor(
    private router: Router, private menuSelector:MenuService, 
    private cartSelector:CartService) { 
    var _self = this;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        _self.ngOnInit();
      }
    })
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
    this.order = this.cartSelector.getOrder();
    (<any>window).gtag('config', 'UA-129699975-4', {'page_path': '/confirm'});

  }

}
