import { ComboOption } from './combo_option.model'
export class ComboSelectable {
    name: string;
    id: number;
    options: Array<ComboOption>;
    base_price: number;
    constructor(selectable){
        this.name = selectable.name;
        this.id = selectable.id;
        this.options = [];
        for(let i in selectable.options){
            selectable.options[i].option_of = this.name;
            let option = new ComboOption(selectable.options[i])
            this.options.push(option);
        }
        this.base_price = selectable.base_price;
            
    }
}

