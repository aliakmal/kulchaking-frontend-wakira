import { CartItem } from './cart_item.model'
import { CartComboItem } from './cart_combo.model';
export class Basket {
    items:Array<CartItem>;
    combos:Array<CartComboItem>;
    subtotal:number;
    delivery_charges:number;
    total:number;

    constructor(){
        this.items = [];
        this.combos = [];
        this.delivery_charges = 5;

        this.updatePrices();
    }

    getCategoriesOfItems(){
        let categories = [];
        for(let i in this.items){
            categories.push(this.items[i].item.menu_section_id);
        }

        return categories;
    }

    initFromBasket(basket){
        for(let i in basket.items){

            let item = new CartItem();
            item.initFromBasketItem(basket.items[i]);
            this.items.push(item);
        }
        for(let i in basket.combos){
            let combo = new CartComboItem();
            combo.initFromBasketCombo(basket.combos[i]);
            this.combos.push(combo);
        }

        this.delivery_charges = basket.delivery_charges;

        this.updatePrices();
    }

    updatePrices(){
        this.updateSubTotal();
        this.updateTotal();
    }

    updateTotal(){
        this.total = this.subtotal+this.delivery_charges;
    }

    outputItemSelecteds(){
        for(let i in this.items){
            console.log(this.items[i].selected_variation);
        }
    }

    addItem(item){
        for(let i in this.items){
            if(this.items[i].id == item.id){
                this.items[i].increment(item.qty);
                this.updatePrices();
                return;
            }
        }
        let cart_item = new CartItem();
        cart_item.initFromBasketItem(item);
        this.items.push(cart_item);
        this.updatePrices();
    }

    count(){
        return (this.items.length + this.combos.length);
    }
    
    removeItems(index){
        this.items.splice(index, 1);
        this.updatePrices();
    }

    incrementItem(index){
        this.items[index].increment(1);
        this.updatePrices();
    }

    decrementItem(index){
        this.items[index].decrement(1);
        this.updatePrices();
    }

    
    addCombo(combo){
        for(let i in this.combos){
            if(this.combos[i].id == combo.id){
                this.combos[i].increment(combo.qty);
                return;
            }
        }
        this.combos.push(combo);
        this.updatePrices();
    }

    removeCombos(index){
        this.combos.splice(index, 1);
        this.updatePrices();
    }

    checkSuggestives(menu_categories){
        let absolute_suggestions = [19,23, 44, 48];// list of categories to ask everytime
        absolute_suggestions = absolute_suggestions.filter(x => menu_categories.includes(x));

        // if category 1 exists but accompaniment doesnt - push accompaniement
        let accompaniment_suggestions = [{category:1, accompaniment:2},{category:1, accompaniment:2}];
        let categories = this.getCategoriesOfItems();
        for(let i = 0; i < absolute_suggestions.length; i++){
            if(categories.indexOf(absolute_suggestions[i]) >= 0){
                absolute_suggestions.splice(i, 1);
            }
        }

        if(absolute_suggestions.length > 0){
            return absolute_suggestions.shift();
        }

        for(let i = 0; i < accompaniment_suggestions.length; i++){
            if(categories.indexOf(accompaniment_suggestions[i].category) >= 0){
                accompaniment_suggestions.splice(i, 1);
            }
        }

        if(absolute_suggestions.length > 0){
            return accompaniment_suggestions[0].accompaniment;
        }

        return false;
    }


    updateSubTotal(){
        this.subtotal = 0;
        for(let i in this.items){
            console.log({thisitems:this.items})
            this.subtotal+= this.items[i].subtotal_price;
        }
        console.log({thissubtotal:this.subtotal})
        for(let i in this.combos){
            this.subtotal+= this.combos[i].subtotal_price
        }
        console.log({thissubtotal:this.subtotal})
    }

}
