import { AddonOption } from './addon_option.model'
export class Addon {
    name: string;
    min:number;
    max:number;
    options:Array<any>;
    selectable_options:Array<any>;

    constructor(addon){
        this.name = addon.name;
        this.min = addon.min == null ? 0 : addon.min;
        this.max = addon.max == null ? 0 : addon.max;
        this.options = addon.options;
        this.selectable_options = [];

        let optionables = addon['options'];

        for(let j in optionables){
            let option = {};
            if(typeof(optionables[j])!='string'){
                continue;
            }
            option['name'] = optionables[j].split('@')[0];
            if(optionables[j].split('@').length > 1){
                option['price'] = optionables[j].split('@')[1];
            }else{
                option['price'] = 0;
            }
            this.selectable_options.push(option);
        }
    }
}

