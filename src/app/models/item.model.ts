import { ItemVariation } from './item_variation.model'
import { Photo } from './photo.model'
import { Addon } from './addon.model'
import { Choice } from './choice.model'

export class Item {
    name: string;
    id: number;
    description: string;
    base_price: number;
    menu_section_id: number;
    created_at: string;
    updated_at: string;
    photo_id: number;
    photo: Photo;
    variations:Array<ItemVariation>;
    addons:Array<Addon>;
    choices:Array<any>;
    selectable_choices:Array<any>;
    flags:string;

    constructor(item){
        this.id = item.id;
        this.name = item.name;
        this.description = item.description;
        this.base_price = item.base_price;
        this.menu_section_id = item.menu_section_id;
        this.created_at = item.created_at;
        this.updated_at = item.updated_at;
        this.photo_id = item.photo_id;
        this.photo = new Photo(item.photo);
        this.variations = [];
        this.addons = [];
        this.choices = [];
        this.flags = typeof(item.flags)!='undefined'?item.flags:'';

        for(let i in item.choices){
            let choice = new Choice(item.choices[i]);
            this.choices.push(choice);
        }

        for(let i in item.variations){
            let variation = new ItemVariation(item.variations[i]);
            this.variations.push(variation);
        }
        for(let i in item.addons){
            let addon = new Addon(item.addons[i]);
            this.addons.push(addon);
        }
    }

    hasVariations(){
        return this.variations.length > 0 ? true : false;
    }

    hasAddons(){
        return this.addons.length > 0 ? true : false;
    }

    getFirstVariation(){
        if(!this.hasVariations()){
            return null;
        }

        return this.variations[0];
    }

}