// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
/*

export const environment = {
  production: false,
  restaurantID: 1508,
  merchantID: 'TEST7005872',
  apiUrl: 'http://wakiradelivers.local/api',
  authCode:'bWVyY2hhbnQuVEVTVDcwMDU4NzI6Y2NjNjExM2VjYmVhNWNhMmQwM2EzNzQ1ZGRkYzRkYmQ='
};
*/
export const environment = {
  production: true,
  restaurantID: 3,
  businessName: 'Kulcha King',
  homeUrl: 'https://www.kulchaking.com',
  logoUrl:'https://s3.ap-south-1.amazonaws.com/www.kulchaking.com/images/logo.png',
  backgroundUrl:'https://s3.ap-south-1.amazonaws.com/www.kulchaking.com/images/1529081139.jpg',

  merchantID: '7005872',//
  // merchantID: 'TEST7005872',
  apiUrl: 'https://www.wakiradelivers.com/api',
  authCode: 'bWVyY2hhbnQuVEVTVDcwMDU4NzI6Y2NjNjExM2VjYmVhNWNhMmQwM2EzNzQ1ZGRkYzRkYmQ=' //
  // authCode: 'bWVyY2hhbnQuVEVTVDcwMDU4NzI6Y2NjNjExM2VjYmVhNWNhMmQwM2EzNzQ1ZGRkYzRkYmQ='
};

